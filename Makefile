OUTPUTS		 = bingen decompile encrypter inserter
DEFS		 = 
OPTIMIZE	 = -O0 -g
LIBS		 = 
LIBDIRS		 = 
INCLUDES	 = -I./
CFLAGS		 = $(INCLUDES) $(OPTIMIZE)
LDFLAGS		 = $(LIBDIRS) $(LIBS) -g
CC		 = gcc
CXX		 = g++
RM		 = rm

.PHONY: clean release all

all: $(OUTPUTS)
%: %.c
	$(CC) $(LDFLAGS) $(CFLAGS) -o $@ $<
clean:
	$(RM) -f $(OUTPUTS)
release: clean
	$(RM) -f ketpdec.bin ketpdec2.bin pgmmus.bin pgmmus.out pgmsong.bin

