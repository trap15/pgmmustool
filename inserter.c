#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

unsigned int xtoi(const char *str)
{
  unsigned int val = 0;
  int i;
  int len = strlen(str);
  for(i = 0; i < len; i++) {
    val <<= 4;
    if(str[i] >= '0' && str[i] <= '9') {
      val |= str[i] - '0';
    }else if(str[i] >= 'A' && str[i] <= 'F') {
      val |= str[i] - 'A' + 0xA;
    }else if(str[i] >= 'a' && str[i] <= 'f') {
      val |= str[i] - 'a' + 0xA;
    }
  }
  return val;
}

int main(int argc, char *argv[])
{
  int c;
  FILE *src, *mus, *dst;
  src = fopen(argv[1], "rb");
  mus = fopen(argv[3], "rb");
  dst = fopen(argv[4], "wb+");
  c = fgetc(src);
  while(!feof(src)) {
    fputc(c, dst);
    c = fgetc(src);
  }
  fseek(dst, xtoi(argv[2]), SEEK_SET);
  c = fgetc(mus);
  while(!feof(mus)) {
    fputc(c, dst);
    c = fgetc(mus);
  }
  fclose(dst);
  fclose(mus);
  fclose(src);
  return EXIT_SUCCESS;
}


