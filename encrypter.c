#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

uint8_t cryptotbl[] =
  { /* Decryption table */
          0x49, 0x47, 0x53, 0x30, 0x30, 0x30, 0x34, 0x52, 0x44, 0x31, 0x30, 0x32, 0x31, 0x30, 0x31, 0x35,
          0x7c, 0x49, 0x27, 0xa5, 0xff, 0xf6, 0x98, 0x2d, 0x0f, 0x3d, 0x12, 0x23, 0xe2, 0x30, 0x50, 0xcf,
          0xf1, 0x82, 0xf0, 0xce, 0x48, 0x44, 0x5b, 0xf3, 0x0d, 0xdf, 0xf8, 0x5d, 0x50, 0x53, 0x91, 0xd9,
          0x12, 0xaf, 0x05, 0x7a, 0x98, 0xd0, 0x2f, 0x76, 0xf1, 0x5d, 0x17, 0x44, 0xc5, 0x03, 0x58, 0xf4,
          0x61, 0xee, 0xd1, 0xce, 0x00, 0x88, 0x90, 0x2e, 0x5c, 0x76, 0xfb, 0x9f, 0x75, 0xcf, 0x40, 0x37,
          0xa1, 0x9f, 0x00, 0x32, 0xd5, 0x9c, 0x37, 0xd2, 0x32, 0x27, 0x6f, 0x76, 0xd3, 0x86, 0x25, 0xf9,
          0xd6, 0x60, 0x7b, 0x4e, 0xa9, 0x7a, 0x20, 0x59, 0x96, 0xb1, 0x7d, 0x10, 0x92, 0x37, 0x22, 0xd2,
          0x42, 0x12, 0x6f, 0x07, 0x4f, 0xd2, 0x87, 0xfa, 0xeb, 0x92, 0x71, 0xf3, 0xa4, 0x31, 0x91, 0x98,
          0x68, 0xd2, 0x47, 0x86, 0xda, 0x92, 0xe5, 0x2b, 0xd4, 0x89, 0xd7, 0xe7, 0x3d, 0x03, 0x0d, 0x63,
          0x0c, 0x00, 0xac, 0x31, 0x9d, 0xe9, 0xf6, 0xa5, 0x34, 0x95, 0x77, 0xf2, 0xcf, 0x7c, 0x72, 0x89,
          0x31, 0x3a, 0x8b, 0xae, 0x2b, 0x47, 0xb6, 0x5d, 0x2d, 0xf5, 0x5f, 0x5c, 0x0e, 0xab, 0xdb, 0xa1,
          0x18, 0x60, 0x0e, 0xe6, 0x58, 0x5b, 0x5e, 0x8b, 0x24, 0x29, 0xd8, 0xac, 0xed, 0xdf, 0xa2, 0x83,
          0x46, 0x91, 0xa1, 0xff, 0x35, 0x13, 0x6a, 0xa5, 0xba, 0xef, 0x6e, 0xa8, 0x9e, 0xa6, 0x62, 0x44,
          0x7e, 0x2c, 0xed, 0x60, 0x17, 0x9e, 0x96, 0x64, 0xd3, 0x46, 0xec, 0x58, 0x95, 0xd1, 0xf7, 0x3e,
          0xc2, 0xcf, 0xdf, 0xb0, 0x90, 0x6c, 0xdb, 0xbe, 0x93, 0x6d, 0x5d, 0x02, 0x85, 0x6e, 0x7c, 0x05,
          0x55, 0x5a, 0xa1, 0xd7, 0x73, 0x2b, 0x76, 0xe9, 0x5b, 0xe4, 0x0c, 0x2e, 0x60, 0xcb, 0x4b, 0x72
  };

uint32_t cryptoalgo[8][7] =
  { /* Decryption algorithm */
    /* &      ==   val     &&   &     ==   val      ^= */
    {0x040480, 0, 0x000080, 0x000000, 1, 0x000000, 0x0001},
    {0x004008, 1, 0x004008, 0x000000, 1, 0x000000, 0x0002},
    {0x080030, 1, 0x000010, 0x000000, 1, 0x000000, 0x0004},
    {0x000042, 0, 0x000042, 0x000000, 1, 0x000000, 0x0008},
    {0x008100, 1, 0x008000, 0x000000, 1, 0x000000, 0x0010},
    {0x002004, 0, 0x000004, 0x000000, 1, 0x000000, 0x0020},
    {0x011800, 0, 0x010000, 0x000000, 1, 0x000000, 0x0040},
    {0x000820, 1, 0x000820, 0x000000, 1, 0x000000, 0x0080},
  };

void crypt(uint16_t* src, uint32_t rom_size, uint8_t table[256], uint32_t algo[8][7])
{
  uint32_t i, l;
  for(i = 0; i < (rom_size / 2); i++) {
    uint16_t x = src[i];
    for(l = 0; l < 8; l++) {
      if(((i & algo[l][0]) == algo[l][2]) == algo[l][1])
        if(((i & algo[l][3]) == algo[l][5]) == algo[l][4])
          x ^= algo[l][6];
    }
    x ^= table[i & 0xff] << 8;
    src[i] = x;
  }
}

void twiddle(uint16_t* src, uint32_t rom_size)
{
  uint32_t i;
  for(i = 0; i < (rom_size / 2); i++) {
    src[i] = (src[i] << 8) | (src[i] >> 8);
  }
}

int main(int argc, char *argv[])
{
  FILE *src, *dst;
  uint16_t *data;
  src = fopen(argv[1], "rb");
  dst = fopen(argv[2], "wb");
  data = malloc(2*1024*1024);

  fread(data, 2, 1024*1024, src);
  twiddle(data, 2*1024*1024);
  crypt(data, 2*1024*1024, cryptotbl, cryptoalgo);
  fwrite(data, 2, 1024*1024, dst);

  fclose(dst);
  fclose(src);
  return EXIT_SUCCESS;
}

