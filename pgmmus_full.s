	.cpu PGMMUS
	.org 0

	.global	fakeSongLength
	.global	fakeVoiceCount
	.global	fakePatternCount
	.global	fakeDataLength

	.global	songLength
	.global	voiceCount
	.global	patternCount
	.global	patternOrder

	.global	startVoices
	.global	voice0
	.global	voice0.pattern0
	.global	voice0.pattern1
	.global	endVoices

fakeSongLength:		.dc.b	$C
fakeVoiceCount:		.dc.b	8
fakePatternCount:	.dc.b	9
fakeDataLength:		.dc.l	$872

songLength:		.dc.b	2
voiceCount:		.dc.b	1
patternCount:		.dc.b	2
patternOrder:		.dc.b	0, 1

startVoices:

voice0:
 .pattern0:
	PLAYFX       $0F, 160, 49, $98
	WAIT         14
	FADE         2
	WAIT         2
	FADE         2
	WAIT         2
	FADE         2
	WAIT         2
	FADE         2
	WAIT         2
	FADE         2
	WAIT         2
	PLAYSMP      31, $01
	WAIT         26
 .pattern1:
	PLAYSMP      30, $01
	WAIT         26
	PLAYSMP      42, $01
	WAIT         2
	OFF         
	WAIT         2
	PLAYSMP      30, $01
	WAIT         26

endVoices:


