#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

struct {
  uint8_t  songlen;
  uint8_t  voices;
  uint8_t  pattcnt;
  uint32_t pattlyt_ptr;
  uint32_t pattord_ptr;
  uint32_t data_ptr;
  uint32_t datalen;
} pgmmus;

uint8_t read8(uint8_t *ptr)
{
  uint8_t v;
  v  = *(ptr+0);
  return v;
}

uint16_t read16(uint8_t *ptr)
{
  uint16_t v;
  v  = *(ptr+0) << 8;
  v |= *(ptr+1) << 0;
  return v;
}

uint32_t read32(uint8_t *ptr)
{
  uint32_t v;
  v  = *(ptr+0) << 24;
  v |= *(ptr+1) << 16;
  v |= *(ptr+2) <<  8;
  v |= *(ptr+3) <<  0;
  return v;
}

void readSongData(uint8_t *data)
{
  pgmmus.songlen     = read8(data+0);
  pgmmus.voices      = read8(data+1);
  pgmmus.pattcnt     = read8(data+2);
  pgmmus.pattord_ptr = read32(data+4);
  pgmmus.pattlyt_ptr = read32(data+8);
  pgmmus.data_ptr    = read32(data+12);
  pgmmus.datalen     = read16(data+16);
}

int decompile_one(uint32_t ea, uint8_t *ops, char *str, int len, int *frames)
{
  int used;
  char line[128];
  char tmp[128];
  char opname[16];
  char oper[4][16];
  uint8_t op;
  *frames += 1;

  used = 1;
  op = ops[0];
  opname[0] = oper[0][0] = oper[1][0] = oper[2][0] = oper[3][0] = '\0';

  switch(op & 0xC0) {
    case 0x00:
      sprintf(opname, "WAIT", op);
      sprintf(oper[0], "%d", op);
      *frames += op;
      break;
    case 0x40:
      switch(op & 0xF) {
        case 0x3:
          switch(ops[1]) {
            case 0x00:
              sprintf(opname, "NOP");
              break;
            default:
              sprintf(opname, "EFFECT");
              sprintf(oper[0], "$%01X", op & 0xF);
              sprintf(oper[1], "$%02X", ops[1]);
              break;
          }
          break;
        case 0xA:
          sprintf(opname, "FADE");
          sprintf(oper[0], "%d", ops[1]);
          break;
        case 0xE:
          switch(ops[1] & 0xF0) {
            case 0xC0:
              sprintf(opname, "OFF");
              break;
            case 0xD0:
              sprintf(opname, "REPEAT");
              break;
            default: // ?
              sprintf(opname, "RELEASE");
              sprintf(oper[0], "%d", ops[1]);
              frames += ops[1];
              break;
          }
          break;
        case 0xF:
          sprintf(opname, "TEMPO");
          sprintf(oper[0], "%d", ops[1]);
          break;
        default:
          sprintf(opname, "EFFECT");
          sprintf(oper[0], "$%01X", op & 0xF);
          sprintf(oper[1], "$%02X", ops[1]);
          break;
      }
      used = 2;
      break;
    case 0x80:
      sprintf(opname, "PLAYSMP");
      sprintf(oper[0], "%d", op & 0x3F);
      sprintf(oper[1], "$%02X", ops[1]);
      used = 2;
      break;
    case 0xC0:
      switch(op & 0x30) {
        case 0x00: case 0x30: // ?
          sprintf(opname, "PLAYFX");
          sprintf(oper[0], "$%02X", op & 0x3F);
          sprintf(oper[1], "%d", ops[1]);
          sprintf(oper[2], "%d", ops[2]);
          sprintf(oper[3], "$%02X", ops[3]);
          used = 4;
          break;
        case 0x10:
          sprintf(opname, ".dc.b");
          sprintf(oper[0], "$%02X", ops[0]);
          sprintf(oper[1], "$%02X", ops[1]);
          sprintf(oper[2], "$%02X", ops[2]);
          used = 3;
          break;
        case 0x20:
          sprintf(opname, ".dc.b");
          sprintf(oper[0], "$%02X", ops[0]);
          sprintf(oper[1], "$%02X", ops[1]);
          sprintf(oper[2], "$%02X", ops[2]);
          used = 3;
          break;
      }
      break;
  }

  if(opname[0] == '\0')
    sprintf(opname, ".dc.b $%02X", op);
  sprintf(tmp, "%-11s ", opname);

  if(oper[0][0] != '\0') {
    sprintf(line, "%s %s", tmp, oper[0]);
    sprintf(tmp, "%s", line);
  }
  if(oper[1][0] != '\0') {
    sprintf(line, "%s, %s", tmp, oper[1]);
    sprintf(tmp, "%s", line);
  }
  if(oper[2][0] != '\0') {
    sprintf(line, "%s, %s", tmp, oper[2]);
    sprintf(tmp, "%s", line);
  }
  if(oper[3][0] != '\0') {
    sprintf(line, "%s, %s", tmp, oper[3]);
    sprintf(tmp, "%s", line);
  }
  snprintf(str, 256, "%s", tmp);

  return used;
}

void decompile_pattern(uint8_t *data, uint32_t ea, uint16_t size)
{
  uint32_t end = ea + size;
  uint32_t used;
  uint8_t *ops;
  char str[256];
  int frames = 0;
  while(ea < end) {
    ops = data + ea;
    ea += decompile_one(ea, ops, str, 256, &frames);

    printf("\t%s\n", str);
    if(frames > 0x40)
      printf("; too large %d!\n", frames);
    if(frames > 0x40)
      frames &= 0x3F;
  }
}

void printSongHeader(uint8_t *data)
{
  int v,p;
  printf(
"\t.cpu\tPGMMUS\n"
"\t.org\t0\n"
"\n"
"\t.global\tsongLength\n"
"\t.global\tvoiceCount\n"
"\t.global\tpatternCount\n"
"\t.global\tpatternOrder\n"
"\n"
"\t.global\tstartVoices\n");

  for(v = 0; v < pgmmus.voices; v++) {
    printf("\t.global\tvoice%d\n", v);
    for(p = 0; p < pgmmus.pattcnt; p++) {
      printf("\t.global\tvoice%d.pattern%d\n", v, p);
    }
  }
  printf(
"\t.global\tendVoices\n"
"\n"
"songLength:\t\t.dc.b\t%d\n"
"voiceCount:\t\t.dc.b\t%d\n"
"patternCount:\t\t.dc.b\t%d\n",
pgmmus.songlen, pgmmus.voices, pgmmus.pattcnt);

  printf("patternOrder:\t\t.dc.b\t");
  for(v = 0; v < pgmmus.songlen; v++) {
    if(v != 0) printf(", ");
    printf("%d", read8(data + pgmmus.pattord_ptr + v));
  }
  printf("\n"
"startVoices:\n");
}

void printSongFooter(uint8_t *data)
{
  printf("\n"
"endVoices:\n\n");
}

void decompile(uint8_t *data, int songdat, int extra)
{
  uint32_t ea;
  uint16_t size;
  int v,p,i;
  readSongData(data + songdat);

  printSongHeader(data);

  ea = pgmmus.data_ptr;
  for(i = 0, v = 0; v < pgmmus.voices; v++) {
    printf("\nvoice%d:\n", v);
    for(p = 0; p < pgmmus.pattcnt; p++, i++) {
      printf("    .pattern%d:\n", p);
      size = read16(data + pgmmus.pattlyt_ptr + (i * 2));
      decompile_pattern(data, ea, size);
      ea += size;
    }
  }

  printSongFooter(data);
}

unsigned int xtoi(const char *str)
{
  unsigned int val = 0;
  int i;
  int len = strlen(str);
  for(i = 0; i < len; i++) {
    val <<= 4;
    if(str[i] >= '0' && str[i] <= '9') {
      val |= str[i] - '0';
    }else if(str[i] >= 'A' && str[i] <= 'F') {
      val |= str[i] - 'A' + 0xA;
    }else if(str[i] >= 'a' && str[i] <= 'f') {
      val |= str[i] - 'a' + 0xA;
    }
  }
  return val;
}

int main(int argc, char *argv[])
{
  int songdat;
  int size;
  uint8_t *data;
  FILE *fp = fopen(argv[1], "rb");
  fseek(fp, 0, SEEK_END);
  size = ftell(fp);
  data = malloc(size);
  fseek(fp, 0, SEEK_SET);
  fread(data, size, 1, fp);
  fclose(fp);

  songdat = xtoi(argv[2]);

  decompile(data, songdat, argc > 3);

  free(data);
  return EXIT_SUCCESS;
}

