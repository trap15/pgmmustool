#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define ES16(v) ((((v) & 0xFF00) >>  8) | \
                 (((v) & 0x00FF) <<  8))

#define ES32(v) ((((v) & 0xFF000000) >> 24) | \
                 (((v) & 0x00FF0000) >>  8) | \
                 (((v) & 0x0000FF00) <<  8) | \
                 (((v) & 0x000000FF) << 24))

#if BIGENDIAN
#define BE16
#define BE32
#else
#define BE16 ES16
#define BE32 ES32
#endif

typedef struct {
  uint32_t strtab_off;
  char name[128];
  uint32_t section;
  uint32_t val;
} symtab_t;

symtab_t syms[65536];
int symcnt = 0;
uint8_t *text;

struct {
  uint8_t  songlen;
  uint8_t  voices;
  uint8_t  pattcnt;
  uint32_t datalen;
  uint8_t  fakesonglen;
  uint8_t  fakevoices;
  uint8_t  fakepattcnt;
  uint32_t fakedatalen;
} pgmmus;

uint32_t get32(FILE *fp)
{
  uint32_t v;
  fread(&v, 4, 1, fp);
  return BE32(v);
}

uint8_t read8(uint8_t *ptr)
{
  uint8_t v;
  v  = *(ptr+0);
  return v;
}

uint16_t read16(uint8_t *ptr)
{
  uint16_t v;
  v  = *(ptr+0) << 8;
  v |= *(ptr+1) << 0;
  return v;
}

uint32_t read32(uint8_t *ptr)
{
  uint32_t v;
  v  = *(ptr+0) << 24;
  v |= *(ptr+1) << 16;
  v |= *(ptr+2) <<  8;
  v |= *(ptr+3) <<  0;
  return v;
}

void fixup_symtab(int s, FILE *ifp)
{
  symtab_t *sym = syms + s;
  int c, i;
  size_t pos = ftell(ifp);
  fseek(ifp, sym->strtab_off, SEEK_CUR);
  c = fgetc(ifp);
  for(i = 0; c > 0; i++) {
    sym->name[i] = c;
    c = fgetc(ifp);
  }
  sym->name[i] = 0;
  fseek(ifp, pos, SEEK_SET);
}

void parse_symtab(FILE *ifp)
{
  symtab_t *sym = syms + symcnt;
  sym->strtab_off = get32(ifp);
  sym->section = get32(ifp);
  sym->val = get32(ifp);
}

int find_symbol(const char *name)
{
  int i;
  for(i = 0; i < symcnt; i++) {
    if(strcmp(syms[i].name, name) == 0) {
      return i;
    }
  }
  return -1;
}

uint32_t symval(const char *name)
{
  int s = find_symbol(name);
  if(s == -1)
    return 0;
  return syms[s].val;
}

void *symptr(const char *name)
{
  int s = find_symbol(name);
  if(s == -1)
    return NULL;
  return text + syms[s].val;
}

void build_metadata(FILE *met)
{
  pgmmus.songlen = read8(symptr("songLength"));
  pgmmus.voices  = read8(symptr("voiceCount"));
  pgmmus.pattcnt = read8(symptr("patternCount"));
  fwrite(&pgmmus.songlen, 1, 1, met);
  fwrite(&pgmmus.voices,  1, 1, met);
  fwrite(&pgmmus.pattcnt, 1, 1, met);

  if(symptr("fakeSongLength"))   pgmmus.fakesonglen = read8(symptr("fakeSongLength"));
  else                           pgmmus.fakesonglen = pgmmus.songlen;
  if(symptr("fakeVoiceCount"))   pgmmus.fakevoices  = read8(symptr("fakeVoiceCount"));
  else                           pgmmus.fakevoices  = pgmmus.voices;
  if(symptr("fakePatternCount")) pgmmus.fakepattcnt = read8(symptr("fakePatternCount"));
  else                           pgmmus.fakepattcnt = pgmmus.pattcnt;
}

void build_songdata(FILE *mus)
{
  int v,p;
  char str[256];
  uint8_t pattorder[256];
  uint8_t *voicedat[16];
  uint16_t pattlyt[256*16];
  uint16_t pattlytswap[256*16];
  uint32_t base;
  uint8_t zero = 0;

  for(p = 0; p < pgmmus.songlen; p++) {
    pattorder[p] = read8(symptr("patternOrder")+p);
  }
  for(; p < pgmmus.fakesonglen; p++) {
    pattorder[p] = 0;
  }

  base = symval("startVoices");
  for(v = 0; v < pgmmus.voices; v++) {
    for(p = 0; p < pgmmus.pattcnt; p++) {
      sprintf(str, "voice%d.pattern%d", v, p);
      pattlyt[(v*pgmmus.pattcnt) + p] = symval(str) - base;
    }
  }
  pattlyt[(pgmmus.voices*pgmmus.pattcnt)] = symval("endVoices") - base;

  pattlyt[(pgmmus.voices*pgmmus.pattcnt)] -= pattlyt[(pgmmus.voices*pgmmus.pattcnt) - 1];
  for(v = pgmmus.voices-1; v >= 0; v--) {
    for(p = pgmmus.pattcnt-1; p >= 0; p--) {
      if(v == 0 && p == 0) continue;
      pattlyt[(v*pgmmus.pattcnt) + p] -= pattlyt[(v*pgmmus.pattcnt) + p - 1];
    }
  }

  for(v = 0; v < pgmmus.voices; v++) {
    for(p = 0; p < pgmmus.pattcnt; p++) {
      pattlyt[(v*pgmmus.pattcnt) + p] = pattlyt[(v*pgmmus.pattcnt) + p + 1];
    }
  }

  for(v = 0; v < pgmmus.voices; v++) {
    for(p = 0; p < pgmmus.pattcnt; p++) {
      pattlytswap[(v*pgmmus.pattcnt) + p] = BE16(pattlyt[(v*pgmmus.pattcnt) + p]);
    }
  }

  for(v = pgmmus.voices*pgmmus.pattcnt; v < (pgmmus.fakevoices*pgmmus.fakepattcnt); v++) {
    pattlyt[v] = 0;
  }

  pgmmus.datalen = symval("endVoices") - symval("startVoices");
  if(symptr("fakeDataLength")) pgmmus.fakedatalen = read32(symptr("fakeDataLength"));
  else                         pgmmus.fakedatalen = pgmmus.datalen;

  fwrite(symptr("startVoices"), pgmmus.datalen, 1, mus);
  for(p = pgmmus.datalen; p < pgmmus.fakedatalen; p++) {
    fwrite(&zero, 1, 1, mus);
  }

  fwrite(pattlytswap, pgmmus.fakepattcnt*2, pgmmus.fakevoices, mus);
  fwrite(pattorder, pgmmus.fakesonglen, 1, mus);
}

int main(int argc, char *argv[])
{
  uint32_t magic;
  uint32_t textsz;
  uint32_t symtabsz;
  int i;

  FILE *ifp = fopen(argv[1], "rb");
  FILE *mus = fopen(argv[2], "wb");
  FILE *met = fopen(argv[3], "wb");
  magic = get32(ifp);
  if(magic != 0407) // omagic
    return EXIT_FAILURE;
  textsz = get32(ifp);
  get32(ifp); // datasz
  get32(ifp); // bsssz
  symtabsz = get32(ifp);
  get32(ifp); // entrypoint
  get32(ifp); // text reloctab sz
  get32(ifp); // data reloctab sz

  text = malloc(textsz);
  fread(text, textsz, 1, ifp);
  for(i = 0; i < symtabsz; i += 0xC) {
    parse_symtab(ifp);
    symcnt++;
  }

  for(i = 0; i < symcnt; i++) {
    fixup_symtab(i, ifp);
  }

  build_metadata(met);
  build_songdata(mus);

  free(text);

  fclose(met);
  fclose(mus);
  fclose(ifp);
  return EXIT_SUCCESS;
}


